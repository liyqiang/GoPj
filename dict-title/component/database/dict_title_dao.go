package database

import (
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/micro/go-micro/util/log"
	"github.com/pku-hit/dict/model/entity"
	"github.com/pku-hit/dict/proto"
	"github.com/pku-hit/dict/util"
	"time"
)

func DeleteTitleDict(id string, soft bool) (err error) {
	dict, err := ExistTitleDictWithId(id)
	if err == gorm.ErrRecordNotFound {
		return
	}

	now := time.Now()
	if soft {
		dict.Status = proto.DictStatus_Deleted.String()
		dict.DeleteAt = &now
		log.Info(util.Json.StructToMap(dict))
		err = db.Model(dict).Select("status", "delete_at").Update(util.Json.StructToMap(dict)).Error
	} else {
		err = db.Where("id = ?", id).Delete(new(entity.DictInfo)).Error
	}
	return
}

func ExistTitleDictWithId(id string) (dict *entity.DictInfo, err error) {
	dict = &entity.DictInfo{}
	err = db.Where("id = ?", id).Find(dict).Error
	if err == gorm.ErrRecordNotFound {
		dict = nil
	}
	return
}

func ExistTitleDict(parentId, code string) (dict *entity.DictInfo, err error) {
	query := db.New()
	if !util.String.IsEmptyString(parentId) {
		query.Where("parent_id = ?", parentId).Where("type in (?)", []string{proto.DictType_Group.String(), proto.DictType_Node.String()})
	} else {
		query.Where("parent_id is null").Where("type = ?", proto.DictType_Root.String())
	}
	query.Where("code = ?", code)
	dict = &entity.DictInfo{}
	err = query.Find(dict).Error

	if err == gorm.ErrRecordNotFound {
		dict = nil
	}
	return
}

func NewTitleDict(category, parentId, code, name, pyCode string, dictType proto.TitleDictType, value interface{}) (dict *entity.DictInfo, err error) {

	if dict, err = ExistTitleDict(parentId, code); err == nil && dict != nil {
		log.Warnf("exist dict: %s", util.Json.ToJsonString(dict))
		return
	}

	now := time.Now()
	dict = &entity.DictInfo{
		ID:       util.Snowflake.GenId(),
		Code:     code,
		Name:     name,
		PyCode:   pyCode,
		Value:    util.Json.ToJsonString(value),
		Status:   proto.DictStatus_Normal.String(),
		CreateAt: &now,
		UpdateAt: &now,
	}

	if util.String.IsEmptyString(parentId) {
		if dictType != proto.TitleDictType_Root {
			err = errors.New("没有指定ParentId的字典，限制仅允许为Root类型")
			return
		}
		dict.Type = proto.TitleDictType_Root.String()
	} else {
		if dictType != proto.TitleDictType_Node && dictType != proto.TitleDictType_Group {
			err = errors.New("指定ParentId的字典，限制仅允许为Group或Node类型")
			return
		}
		if _, err = ExistDictWithId(parentId); err == gorm.ErrRecordNotFound {
			err = errors.New("指定的父节点不存在")
			return
		}
		dict.ParentId = parentId
		dict.Type = dictType.String()
	}

	if !util.String.IsEmptyString(category) {
		dict.Category = category
	}

	err = db.Save(dict).Error
	if err != nil {
		log.Error("save new dict error %s.", err.Error())
	}
	return
}

func GetHollTitleDict()(dicts []*entity.DictInfo,err error)  {
	query := db.New()
	err = query.Find(&dicts).Error
	return
}

func GetTitleDictByID(id string) (dict *entity.DictInfo,err error){
	dict = &entity.DictInfo{}
	err = db.Where("id = ?", id).Find(dict).Error
	if err == gorm.ErrRecordNotFound {
		dict = nil
	}
	return
}



